/*
Name of the project : Datastructure_matrices
File name : array_2/sum.js
Description : finds the product of digits in a number.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : 123
Output : 6
*/

var input=123;
var result = 1;
var rem;
var val=input;
while(val) {
  rem = val % 10;
  result *= rem;
  val = Math.floor(val / 10);
  //val = parseInt(val);
 }
console.log(result);
