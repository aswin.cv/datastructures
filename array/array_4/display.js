/*
Name of the project : Datastructure_matrices
File name : array_4/display.js
Description : returns elements in pendulam moveent form.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [2,5,4,3,1]
Output : [4,2,1,3,5]
*/


function find(input){
    var out = [];
    var temp;
    //sorting the array
    for (var i = 0; i < input.length; i++) {
        for (var j = 0; j < input.length-i-1; j++) {
            if( input[j] > input[j+1]){
                temp = input[j];
                input[j] = input[j+1];
                input[j+1] = temp;
            }
        }
    }
    var i,j,k;
    //arrange the elements in pendulam movement order
    k = Math.floor(input.length/2);
    j = k-1;
    for( i=0; i<input.length; i++) {
        if(i==0) {
            out[k++] = input[i];
        }else {
            if( i%2 == 0) {
              out[k++] = input[i];
            }else {
              out[j--] = input[i];
            }
        }
    }
    return out;
}


console.log(find([2,5,4,3,1]));
