/** Node class represent the datatype for  the tree node which contains value feild for data &
 * left feild for left node &right field for right node
 */
class Node {
  constructor(data) {
    this.left = null;
    this.value = data;
    this.right = null;
  }
}

/**getBinaryTree() returns the binary search tree */
function getBinaryTree() {
  root = new Node(8);
  root.left = new Node(3);
  root.right = new Node(10);
  root.left.left = new Node(1);
  root.left.right = new Node(6);
  root.left.right.left = new Node(4);
  root.left.right.right = new Node(7);
  root.right.right = new Node(14);
  root.right.right.left = new Node(13);
  return root;
}

/**getMinElement() returns the minimum element of the tree */
function getMinElement(root, minItem) {
  if (root == null)
    return minItem;
  if (root.value < minItem)
    minItem = root.value;
  minItem = getMinElement(root.left, minItem);
  minItem = getMinElement(root.right, minItem);
  return minItem
}

/**maxDiff() function takes root of the node as input and return the maximum difference .
 * here the concept is for each node find the minimum element to its left and right child 
 * calculatte the difference whichever is greater update with maximum value .
 * & repeat thi process for each node
 */
function maxDiff(root, result) {
  if (root == null)
    return 0;
  let leftMinElement = getMinElement(root.left, Number.MAX_SAFE_INTEGER);
  let rightMinElement = getMinElement(root.right, Number.MAX_SAFE_INTEGER);
  //console.log(leftMinElement, rightMinElement);
  result = Math.max(root.value - leftMinElement, root.value - rightMinElement, result);
  maxDiff(root.left, result);
  maxDiff(root.right, result);
  return result;
}

/**run() initialize the binary tree and call maxDiff() function */
function run() {
  let root = getBinaryTree();
  let result = Number.MIN_SAFE_INTEGER;
  result = maxDiff(root, result);
  console.log(result);


}