/*
Name of the project : Datastructure_matrices
File name : matrices_4/display.js
Description : to print the array 90degerre rotated
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [[0,3,1,0],[3,0,3,3],[2,3,0,3],[0,3,3,3]];
Output : 0: (4) [0, 3, 3, 3]
         1: (4) [1, 3, 0, 3]
         2: (4) [3, 0, 3, 3]
         3: (4) [0, 3, 2, 0]
*/
var R = 4;
var C = 4;

var arr=[[0,3,1,0],[3,0,3,3],[2,3,0,3],[0,3,3,3]];
function find()
{
    transpose();
    reverseColumns();
    console.log(arr);
}

// After transpose we swap elements of column
// one by one for finding left rotation of matrix
// by 90 degree
function reverseColumns() {
    for (var i = 0; i < C; i++)
        for (var j = 0, k = C - 1; j < k; j++, k--)
        {
            temp=arr[k][i];
            arr[k][i]=arr[j][i];
            arr[j][i]=temp;
        }
}

// Function for do transpose of matrix
function transpose()
{
    var temp;
    for (var i = 0; i < R; i++)
        for (var j = i; j < C; j++)
            {
                temp=arr[i][j];
                arr[i][j]=arr[j][i];
                arr[j][i]=temp;
            }
}


find();
