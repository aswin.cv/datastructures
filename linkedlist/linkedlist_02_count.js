/**addNode() function adds node at last  */
function addNode(head, data) {
  var node = {
    value: data,
    next: null
  };
  if (head === null) {
    return node;
  }
  else if (head.next === null) {
    head.next = node;
  }
  else {
    var temp = head;
    while (temp.next != null) {
      temp = temp.next;
    }
    temp.next = node;
  }

}
/**count() function will count the no of times a value occur in the list which takes 
 * head of the list as input  */
function count(head) {
  let map = new Map();
  var temp = head;
  while (temp != null) {
    var itemCount = 1;
    if (!map.has(temp.value)) {
      var temp1 = head;

      while (temp1 != null) {
        if (temp != temp1)
          if (temp.value === temp1.value)
            itemCount++;

        temp1 = temp1.next;
      }
      map.set(temp.value, itemCount);
    }
    temp = temp.next;
  }
  console.log(map);
}

/**printList() function just traverse the list */
function printList(head) {
  var temp = head;
  while (temp != null) {
    console.log(temp.value);
    temp = temp.next;
  }
}

/**run() function will be invoked when user click the run button on browser.
 * & initialize the list & invokes the count() method
 */
function run() {
  let head = addNode(null, 17);
  addNode(head, 15);
  addNode(head, 17);
  addNode(head, 9);
  addNode(head, 2);
  addNode(head, 17);
  addNode(head, 9);
  console.log("List Items");
  printList(head);
  count(head);

}



