/**addNode() fuhnction will add the node to the tree which takes value of the node as input */
function addNode(data) {
  var node = {
    value: data,
    left: null,
    rigth: null
  };
  return node;
}

/*sortNumber() will return the difference of the two number which is invoked by javascript sort method
 for each element to sort the array element * */
function sortNumber(a, b) {
  return a - b;
}

/**merge function will print the values of the two BSTs in sorted order */
function merge(root1, root2) {
  var items = [];
  if (root1 == null)
    items = inorder(root2, items);
  if (root2 == null)
    items = inorder(root2, items);
  else {
    items = inorder(root1, items);
    //console.log(items);
    items.concat(inorder(root2, items));
    //console.log(items);
    items = items.sort(sortNumber);
    // console.log(items)
  }
  return items;
}

/**inorder() funcrtion traverse the tree in inorder fashion and takes the root of the tree as input */
function inorder(root, items) {
  if (root == null)
    return items;
  if (root.left != null)
    inorder(root.left, items);
  //console.log(root.value);
  items.push(root.value);
  if (root.right != null)
    inorder(root.right, items);
  return items
}
/**run() function will initialize the binary search tree &data to be deleted
 * and invokes the merge() function
 */
function run() {
  var root1 = addNode(100);
  root1.left = addNode(50);
  root1.right = addNode(300);
  root1.left.left = addNode(20);
  root1.left.right = addNode(70);

  var root2 = addNode(80);
  root2.left = addNode(40);
  root2.right = addNode(120);
  var items = merge(root1, root2);
  console.log(items);

}