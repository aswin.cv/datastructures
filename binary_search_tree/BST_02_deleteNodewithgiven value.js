/**addNode() fuhnction will add the node to the tree which takes value of the node as input */
function addNode(data) {
  var node = {
    value: data,
    left: null,
    rigth: null
  };
  return node;
}

/** getBinaryTree() returns the binary search tree */
function getBinaryTree() {
  root = addNode(4);
  root.left = addNode(2);
  root.right = addNode(5);
  root.left.left = addNode(1);
  root.left.right = addNode(3);
  root.right.right = addNode(6);
  root.right.right.right = addNode(7);
  root.right.right.right.right = addNode(8);
  return root;
}

/**getNode() function takes root of tree & data to be deleted as input and returns the adress of 
 * the node to be deleted  */
function getNode(root, data) {
  var flag = 0;
  var templ = null;
  var tempr = null
  if (root == null)
    return null;

  if ((root.left != null && root.left.value == data) || (root.right != null && root.right.value == data))
    return root;
  if (root.left != null)
    templ = getNode(root.left, data);
  if (root.right != null)
    tempr = getNode(root.right, data);

  if (templ == null && tempr != null)
    return tempr;
  if (templ != null && tempr == null)
    return templ;
  return null;
}

/**getAncestor() will take a node(either can be a root node or internal node ) as input 
 * and returns the ancestor(means smallest value node in the tree ) of the node  */
function getAncestor(node) {
  if (node == null)
    return null;
  else if (node.right == null)
    return null;
  while (node.right.right != null)
    node = node.right;
  var temp = node.right;
  node.right = null;
  return temp;
}

/**getPredecessor() will take a node(either can be a root node or internal node ) as input
 * and returns the Predecessor(means largest value node in the tree ) of the node  */
function getPredecessor(node) {
  if (node == null)
    return null;
  else if (node.left == null)
    return null;
  while (node.left.left != null)
    node = node.left;
  var temp = node.left;
  node.left = null;
  return temp;
}

/** this function deletes the node with given data 
 * To delete a node first we find the node with given data by invoking getNode() method after
 *  getting the node we get its ancestor and predecessor by using getAncestor() &getPredecessor() method
 * and than delete the node and fill that place with either its ancestor or its predecessor
*/
function deleteNode(root, data) {
  if (root.value == data) {
    var ancestor = getAncestor(root.left);
    var predecessor = getPredecessor(root.right);
    if (ancestor == null && predecessor == null) {
      var temp = root;
      if (root.right != null) {
        root = root.right;
        root.left = temp.left;
      }
      return root;
    }
    else if (ancestor != null && predecessor == null) {
      var temp = root;
      root = ancestor;
      root.left = temp.left;
      root.right = temp.right;
      return root;

    }
    else {
      var temp = root;
      root = predecessor;
      root.left = temp.left;
      root.right = temp.right;
      return root;

    }

  }
  var temp = getNode(root, data);
  var deletingNode = (temp.left != null && temp.left.value == data) ? temp.left : temp.right;
  var ancestor = getAncestor(deletingNode.left);
  var predecessor = getPredecessor(deletingNode.right);
  console.log(ancestor + "...." + predecessor);
  if (ancestor == null && predecessor == null)
    if (temp.left != null && temp.left.value == data)
      temp.left = deletingNode.left;
    else //if (temp.right != null && temp.right.value == data)
      temp.right = deletingNode.right;
  else if (ancestor == null && predecessor != null) {
    predecessor.left = deleting.left;
    predecessor.right = deleting.right;
    if (temp.left != null && temp.left.value == data)
      temp.left = predecessor;
    else //if (temp.right != null && temp.right.value == data)
      temp.right = predecessor;
  }
  else if (ancestor != null && predecessor == null) {
    ancestor.left = deleting.left;
    ancestor.right = deleting.right;
    if (temp.left != null && temp.left.value == data)
      temp.left = ancestor;
    else //if (temp.right != null && temp.right.value == data)
      temp.right = ancestor;
  }
  else {
    predecessor.left = deleting.left;
    predecessor.right = deleting.right;
    if (temp.left != null && temp.left.value == data)
      temp.left = predecessor;
    else //if (temp.right != null && temp.right.value == data)
      temp.right = predecessor;
  }

  return root;
}

/**inorder() funcrtion traverse the tree in inorder fashion and takes the root of the tree as input */
function inorder(root) {
  if (root == null)
    return;
  if (root.left != null)
    inorder(root.left);
  console.log(root.value);
  if (root.right != null)
    inorder(root.right);

}

/**run() function will initialize the binary search tree &data to be deleted
 * and invokes the deletenode() function
 */
function run() {
  var root = getBinaryTree();
  console.log("Inorder traversal of tree before deleting the node");
  inorder(root);
  var data = 4;
  root = deleteNode(root, data);
  console.log("Inorder traversal of tree after deleting the node");
  inorder(root);
}